function natoText = textToNato(plainText)
    natoText = '';
    for i = 1:length(plainText)
        switch upper(plainText(i))
            case 'A'
                natoText = strcat(natoText,'-', 'Alpha');
            case 'B'
                natoText = strcat(natoText,'-', 'Bravo');
            case 'C'
                natoText = strcat(natoText,'-', 'Charlie');
            case 'D'
                natoText = strcat(natoText,'-', 'Delta');
            case 'E'
                natoText = strcat(natoText,'-', 'Echo');
            case 'F'
                natoText = strcat(natoText,'-', 'Foxtrot');
            case 'G'
                natoText = strcat(natoText,'-', 'Golf');
            case 'H'
                natoText = strcat(natoText,'-', 'Hotel');
            case 'I'
                natoText = strcat(natoText,'-', 'India');
            case 'J'
                natoText = strcat(natoText,'-', 'Juliet');
            case 'K'
                natoText = strcat(natoText,'-', 'Kilo');
            case 'L'
                natoText = strcat(natoText,'-', 'Lima');
            case 'M'
                natoText = strcat(natoText,'-', 'Mike');
            case 'N'
                natoText = strcat(natoText,'-', 'November');
            case 'O'
                natoText = strcat(natoText,'-', 'Oscar');
            case 'P'
                natoText = strcat(natoText,'-', 'Papa');
            case 'Q'
                natoText = strcat(natoText,'-', 'Quebec');
            case 'R'
                natoText = strcat(natoText,'-', 'Romeo');
            case 'S'
                natoText = strcat(natoText,'-', 'Sierra');
            case 'T'
                natoText = strcat(natoText,'-', 'Tango');
            case 'U'
                natoText = strcat(natoText,'-', 'Uniform');
            case 'V'
                natoText = strcat(natoText,'-', 'Victor');
            case 'W'
                natoText = strcat(natoText,'-', 'Whiskey');
            case 'X'
                natoText = strcat(natoText,'-', 'Xray');
            case 'Y'
                natoText = strcat(natoText,'-', 'Yankee');
            case 'Z'
                natoText = strcat(natoText,'-', 'Zulu');
        end
    end
    if (strcmp(natoText(1), '-'))
        natoText = natoText(2:length(natoText));
    end
end