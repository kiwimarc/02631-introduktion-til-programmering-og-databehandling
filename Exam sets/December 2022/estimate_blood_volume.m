function ebv = estimate_blood_volume(weight, group)
    group = string(group); % Makes sure that group is a string

    % Creating dictionary with the given keys and values
    keys = ["P", "N", "I", "M", "F"];
    values = [95,85,80,75,65];
    ABV = containers.Map(keys,values);
    
    try
        ebv = weight * ABV(group);
    catch ME
        switch ME.identifier
            case 'MATLAB:Containers:Map:NoKey'
                disp("Not a valid group");
            case 'MATLAB:UndefinedFunction'
                disp("Weight is not number");
        end
        ebv = nan;
    end
end