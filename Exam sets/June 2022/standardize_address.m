function s = standardize_address(a)
    a = string(a);
    if any(count(a," "))
        a = split(a," ");
        index = contains(a, digitsPattern);
        s = join([a(index); a(index==0)]," ");
    elseif any(count(a,"_"))
        a = split(a,"_");
        index = contains(a, digitsPattern);
        s = join([a(index); a(index==0)]," ");
    end
end