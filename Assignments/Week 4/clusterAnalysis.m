function clusterAssignments = clusterAnalysis(reflectance)
    
    m1 = [];
    m2 = [];
    % Split array up in odd and even index
    for i = 1:length(reflectance)
        % if odd
        if(mod(i,2) == 0) 
            m2 = [m2 reflectance(i)];
        % if even
        elseif(mod(i,2) ~= 0)
            m1 = [m1 reflectance(i)];
        end
    end
    
    prem1 = [];
    prem2 = [];

    while (~isequal(m1,prem1) && ~isequal(m2,prem2))
        % Save odd and even from previus run
        prem1 = m1;
        prem2 = m2;

        % Calculate means
        mean1 = mean(m1);
        mean2 = mean(m2);

        % Empty lists
        m1 = [];
        m2 = [];

        % prem1 loop
        for i = 1:length(prem1)
            
            dist1 = abs(prem1(i) - mean1);
            dist2 = abs(prem1(i) - mean2);

            if (dist1 > dist2)
                 m2 = [m2 prem1(i)];
            else
                m1 = [m1 prem1(i)];
            end
        end
        
        % prem2 loop
        for i = 1:length(prem2)
            
            dist1 = abs(prem2(i) - mean1);
            dist2 = abs(prem2(i) - mean2);

            if (dist1 > dist2)
                 m2 = [m2 prem2(i)];
            else
                m1 = [m1 prem2(i)];
            end
        end
    end

    for i = 1:length(reflectance)
        if ~any(m1 == reflectance(i))
            index = find(reflectance == reflectance(i));
            clusterAssignments(index) = 2;

        elseif ~any(m2 == reflectance(i))
            index = find(reflectance == reflectance(i));
            clusterAssignments(index) = 1;
        end
    end
end