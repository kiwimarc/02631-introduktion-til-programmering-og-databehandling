function price = calculate_apple_price(x)
    x = double(x); % Makes sure its the right data type
    price = 0;
    while (x > 0)
        % Case 1
        if (x > 10)
            for i = 1:(x/10)
                x = x-10;
                price = price + 34;
            end
        % Case 2  
        elseif (x > 5 || (x/5)*5 < 34)
            for i = 1:round(x/5)
                x = x-5;
                price = price + 20;
            end
        else
            for i = 1:x
                x = x-1;
                price = price + 6;
            end 
        end
    end
end