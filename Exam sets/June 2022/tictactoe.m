function score = tictactoe(board)
    found = 0;
    for i = 1:2
        if (sum(board(1,:) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(2,:) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(3,:) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(:,1) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(:,2) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(:,3) == i) == 3)
            score = i;
            found = found + 1;
        end
        if (sum(board(1,1) == i) == 1 && sum(board(2,2) == i) == 1 && sum(board(3,3) == i) == 1)
            score = i;
            found = found + 1;
        end
        if (sum(board(3,1) == i) == 1 && sum(board(2,2) == i) == 1 && sum(board(1,3) == i) == 1)
            score = i;
            found = found + 1;
        end
    end
    if (found > 1)
        score = -1;
        return
    end
    if ((found == 1) && ((sum(sum(board == 1)) == sum(sum(board == 2))) || ...
            sum(sum(board == 1)) == sum(sum(board == 2)) + 1))
        return
    elseif (sum(sum(board == 1)) == sum(sum(board == 2)) || ...
            sum(sum(board == 1)) == sum(sum(board == 2)) + 1)
        score = 0;
    else
        score = -1;
    end
end