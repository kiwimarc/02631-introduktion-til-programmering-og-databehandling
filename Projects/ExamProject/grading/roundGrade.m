function gradesRounded = roundGrade(grades)
    % Round off each element in the vector grades and return the nearest
    % grade on the 7-step-scale
    %
    % Usage: gradesRounded = roundGrade(grades)
    % 
    % Input grades: An N × M matrix (table) containing grades (doubles)
    %                     for N students on M different assignments
    % Output gradesRounded: A N long vector of doubles
    
    scale = [0,2,4,7,10,12];
    
    gradesRounded = [];

    for i = 1:length(grades)
        % checks if number is on the scale
        if ismember(grades(i),scale)
            gradesRounded(end+1) = grades(i);
        % special case for negative numbers
        elseif (grades(i) < 0)
            if (grades(i) + 3/2 > 0)
                gradesRounded(end+1) = 0;
            else
                gradesRounded(i) = -3;
            end
        elseif isnan(grades(i))
            gradesRounded(end+1) = nan;
        % positive numbers
        else
            % creates af difflist which will always contain the smallest
            % difference itterated over and the belonging grade
            difflist = [ abs(grades(i)-scale(1)), scale(1)];
            % checks for each number if the difference is smaller that what
            % is kept in difflist
            % This will always round down in case of tie
            for k = 2:(length(scale)-1)
                difference = abs(grades(i)-scale(k));
                if difflist(1) > difference
                    difflist = [difference, scale(k)];
                end
            gradesRounded(i) = difflist(2);
            end
        end
    end

end