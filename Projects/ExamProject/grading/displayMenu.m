function choice = displayMenu(options)
    % Orginal forfatter: Mikkel N. Schmidt, mnsc@dtu.dk, 2015
    % Ændret af: Marc Cummings
    % Displays a menu of options in the terminal, asks the user to
    % choose an option and returns the number of the menu item chosen.
    %
    % Usage: choice = displayMenu(options)
    %
    % Input options: Menu options (cell array of strings)
    % Output choice: Chosen option (integer)

    % Display menu options
    for i = 1:length(options)
        disp(string(i)+'. '+options{i});
    end
    % Get a valid menu choice
    choice = '0';
    firstTime = true;
    while ~ismember(choice,string(1:length(options)))
        % Message for invalid input
        if ~firstTime
            disp("Not an option");
        end
        choice = input('Please choose a menu item: ', 's');
        firstTime = false;
    end
    choice = str2num(['uint8(',choice,')']);
end