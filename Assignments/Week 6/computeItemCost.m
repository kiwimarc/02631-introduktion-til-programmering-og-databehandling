function itemCost = computeItemCost(resourceItemMatrix, resourceCost)
    n = length(resourceItemMatrix(1,:));
    itemCost = zeros(1,n);

    for i = 1:n
        itemCost(i) = sum(resourceItemMatrix(:, i) .* resourceCost(:));
    end
end