function angle = time_angle(hour, minute)
    if hour > 12
        hour = hour - 12;
    end
    if (hour ~= 12)
        aHour = 1/12*(hour+minute/60)*360;
        aMin = (minute/60)*360;
        angle = aHour - aMin;
    else
        angle = 0;
    end
    
    if angle < 0
        angle = angle + 360;
    end
end