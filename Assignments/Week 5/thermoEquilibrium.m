function t = thermoEquilibrium(N, r)
    Nl = N;
    Nr = 0;
    t = 1;
    
    % Run through all random numbers
    for i = 1:length(r)
        pLR = Nl/N;
        
        % If the random number is smallere than the probaility of a particle will move from the left to the right chamber
        if(r(i) <= pLR)
            Nl = Nl - 1;
            Nr = Nr + 1;
            
        elseif(~(Nl == 0))
            Nl = Nl + 1;
            Nr = Nr - 1;
        end

        if (Nl == Nr)
            return
        end
        t = t + 1;
    end
    if (Nl ~= Nr)
        t = 0;
    end
end
