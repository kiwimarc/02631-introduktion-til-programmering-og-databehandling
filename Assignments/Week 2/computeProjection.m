function projection = computeProjection(a)
    
    b = ones(1,length(a));
    top = 0;
    bottom = a(1)^2;
    if length(a) == 2
        for i = 2:length(a)
            top = top + (a(i - 1) * b(i - 1) + a(i) * b(i));
            bottom = bottom + a(i)^2;
        end
        projection = (top / bottom) * a;
    else
        projection = (dot(a,b)/norm(a)^2)*a;
    end
end