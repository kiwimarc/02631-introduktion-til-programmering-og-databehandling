a = 2;
b = -5;
c = 2;

% Compute the smallest solution
x1 = (-b-sqrt(b^2-4*a*c))/(2*a);
% Compute the largest solution
x2 = (-b+sqrt(b^2-4*a*c))/(2*a);

% NOT IN THE ASSIGNMENT ONLY TO CHECK RESULT
disp(x1)
disp(x2)