function date = convert_date(day, month, year, template)
    template = string(template); % Makes sure its a string
    template = replace(template,"dd",string(datetime([double(year),double(month),double(day)],'Format',"dd")));
    template = replace(template,"mm",string(datetime([double(year),double(month),double(day)],'Format','M')));
    template = replace(template,"MM",string(datetime([double(year),double(month),double(day)],'Format','MMM')));
    template = replace(template,"yy",string(datetime([double(year),double(month),double(day)],'Format','yyyy')));

    date = template;
end