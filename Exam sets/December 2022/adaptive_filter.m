function y = adaptive_filter(s)
    r = zeros(1,length(s));
    y = zeros(1,length(s));
    % Calculation radius
    for i = 1:length(s)
        r(i) = floor(log10(s(i)^2 + 1 )) +1;
    end
    % Calculation y
    for i = 1:length(s)
        radius = r(i);
        arr = [];
        for l = (i-radius):(i+radius)
            if l <= 0
                arr = [arr, 0];
            elseif l > length(s)
                 arr = [arr, 0];
            else 
                arr =  [arr, s(l)];
            end
        end
        y(i) = sum(arr)/(2*radius+1);
    end
end