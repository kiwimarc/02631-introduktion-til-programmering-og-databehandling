function score = computePassesGoalLine(point, directionVector)
    xgoal = 105;       % Given cross x-coordinate
    xstart = 0;        % Given beginning x-coordinate
    goalstart = 30.34; % Given beginning y-coordinate
    goalstop = 37.66;  % Given end y-coordinate
    
    % Check if the direction vector is negativ
    if (directionVector(1) < 0)
        alfa = (xstart - point(1)) / directionVector(1); % Computing how far the ball should travel along v
        ygoal = point(2) + (alfa * directionVector(2));  % Computing where the ball passes the goal line

    elseif (directionVector(1) > 0)
        alfa = (xgoal - point(1)) / directionVector(1); % Computing how far the ball should travel along v
        ygoal = point(2) + (alfa * directionVector(2)); % Computing where the ball passes the goal line
    
    % If direction is not towards any of the goals
    else
        score = false;
        return;
    end

    % Check if the ball ends up in the goal
    if (goalstart < ygoal && goalstop > ygoal)
        score = true;

    else
        score = false;
    end
end