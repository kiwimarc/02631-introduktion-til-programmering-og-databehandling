function h = water_height(h0, r)
    if (isempty(r))
        h = h0;
        return
    end
    hToday = 0;
    hYesterday = h0;
    for i = 1:length(r)
        hToday = hYesterday + r(i) - 2;
        if (hToday < 0 )
            hToday = 0;
        end
        hYesterday = hToday;
    end
    h = hToday;
end