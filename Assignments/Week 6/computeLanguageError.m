function E = computeLanguageError(freq)
    letterFrequency = csvread('letter_frequencies.csv',1,1);
    E = zeros(1,15);

    for i = 1:15
        for l = 1:26
            E(i) = E(i) + (freq(l) - letterFrequency(l, i))^2;
        end
    end
end