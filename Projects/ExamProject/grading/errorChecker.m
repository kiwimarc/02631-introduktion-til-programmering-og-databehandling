function errorChecker(data)
    % Checks for error in data and displays errors to user
    %
    % Usage: errorChecker(data)
    % 
    % Input data: data (array of doubles)
    % Output error(s): string(s) of informations in console

    % Instantiate variables
    % Define grades in the 7 grade scale
    posibleGrades = [-3,0,2,4,7,10,12];
    
    % Converts studentID coloumn to array 
    studentID = table2array(data(:,1));
    % Find the unique rows, along with indices for identifying the duplicates
    [uniqueTableRows,indexToUniqueRows] = unique(studentID);
    % Find index of the duplicatet student ids
    duplicateStudentID = find(not(ismember(1:numel(studentID),indexToUniqueRows)));
    % if found duplicate(s), tell user where the duplicate(s) are located
    if any(duplicateStudentID)
        disp("######################################################");
        disp("Following rows has duplicated student ID's");
        rows = find(studentID == string(studentID(duplicateStudentID(1))));
        disp("Row: "+rows(1)+" is duplicated in");
        for i = 1:length(rows)-1
            disp("- Row: "+rows(i+1));
        end
    end
    if isempty(data)
        disp("######################################################");
        disp("#                     Is empty                       #");
        disp("######################################################");
        return
    end
    % Go through all assigments
    for i = 1:(width(data(1,:))-2)
        assigmentGrades = table2array(data(:,i+2));
        % Go through all grades of each assigment and
        % find index of all grades not posible
        errorGrades = not(ismember(assigmentGrades,posibleGrades));
        % if found error(s), tell user where the error(s) are located
        if any(errorGrades)
            disp("######################################################");
            rows = find(errorGrades == 1);
            disp("Assigment "+i);
            disp("Following rows has grades not on the 7 grade scale:");
            for l = 1:length(rows)
                % If nan is found, then is posible that a grade is missing
                if isnan(assigmentGrades(rows(l)))
                    disp("- Row: "+rows(l)+" (Not a number," + ...
                                           " posible a missing grade)");
                elseif(isnumeric(assigmentGrades(rows(l))))
                    disp("- Row: "+rows(l));
                % If not numerice, then is posible a string mishab
                else
                    disp("- Row: "+rows(l)+" (Not numeric," + ...
                                            "posible a string mishab)");
                end
            end
        end
    end

end