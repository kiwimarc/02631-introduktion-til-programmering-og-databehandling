function dataPlot(data)
    % Display two different kind of plots, depending on what the user
    % chooses.
    % The first plot is a bar plot that shows the number of each of the 
    % different types of Bacteria in the data.
    % The second plot is a line plot that show the Growth rate as a
    % function of temperature.
    %
    % Usage: dataPlot(data)
    % 
    % Input data: data (array of doubles)
    % Output No output, but produces two different kind of plots, 
    % depending on what the user chooses.

    % Instantiate variables
    %
    % Define menu items
    menuItems = {'Number of bacteria', 'Growth rate by temperature', 'Quit'};
    % Define bacterias
    bacterias = ["Salmonella enterica" "Bacillus cereus" "Listeria" "Brochothrix thermosphacta"];
    % Define bacterias number
    bacteriasNumber = [1 2 3 4];
    
    % Get users choice
    choice = displayMenu(menuItems);
    
    % A bar plot of the number of each of the different types of
    % Bacteria in the data. 
    if (choice == 1)
        % Categorical array from the array bacterias, but only hold the
        % bacterias that are present in dataset
        x = categorical(bacterias(bacteriasNumber( ...
                ismember(bacteriasNumber, data(:,3)) ... % Get index of present bacterias
            )));
        y = [];
        % Add data to each bar
        for i = 1:length(bacteriasNumber)
            n = sum(data(:,3) == i);
            if (n ~= 0)
                y = [y n];
            end
        end
        % Create a new figure window
        figure;
        % Plot bar plot
        bar(x, y);
        % Set the title of the graph depending on if 
        % its one bacteria or more
        if (length(y) > 1)
            title('Number of datapoint per type of bacteria');
        else
            title('Bar plot of '+ string(x));
        end
        
    % A plot with the Temperature on the x-axis and the
    % Growth rate on the y-axis.
    % x-axis goes from 10 to 60 degrees
    % y-axis starts at 0
    elseif (choice == 2)
        % Get indexes of present bacterias
        bacterieIndexes = bacteriasNumber( ...
                ismember(bacteriasNumber, data(:,3)) ...
            );
        % sorts the rows in ascending order based on temperature
        plotData = sortrows(data);
        % Create a new figure window
        figure;
        % Set the title of the graph
        title('Growth rate as a function of temperature');
        % Set the x-axis label
        xlabel('Temperature');
        % Set the y-axis label
        ylabel('Growth rate');
        % Set the limits of the x-axis
        xlim([10, 60]);
        % Add legend
        legend();
        % Set max height to 0
        maxY = 0;
        % Retain current plot when adding new plots
        hold on;
        % Create dataset for each present bacteria
        for i = 1:length(bacterieIndexes)
            x = [plotData(plotData(:,3)==bacterieIndexes(i),1);];
            y = [plotData(plotData(:,3)==bacterieIndexes(i),2);];
            % y's max heigth is bigger than the old max heigth
            if (max(y) > maxY)
                % Set the limits of the y-axis
                ylim([0, max(y)]);
                maxY = max(y);
            end
            % Plot line graph of x and y
            plot(x, y, 'DisplayName', bacterias(i));
        end
        hold off;
        
    % Go back
    elseif(choice == 3)
        return;
    end
end