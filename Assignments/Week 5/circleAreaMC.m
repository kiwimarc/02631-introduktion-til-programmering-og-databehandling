function A = circleAreaMC(xvals, yvals)
    Asqr = 4;
    n = 0;
    
    % foreach cordinate set
    for i = 1:length(xvals)
        l = sqrt(xvals(i)^2+yvals(i)^2);
        
        % If the length of the vector is smaller than 1
        if(l < 1)
            n = n + 1;
        end
    end

    A = Asqr * n/length(xvals);
end