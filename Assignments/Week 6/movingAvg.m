function ysmooth = movingAvg(y)
    n = length(y);
    % Special case for n = 1
    if (n == 1)
        r1 = [0];
        r2 = [0] * 2;
        r3 = [y] * 3;
        r4 = [0] * 2;
        r5 = [0];
    else
        r1 = [y(3:n),zeros(1,2)];
        r2 = [y(2:n),zeros(1,1)] * 2;
        r3 = [y] * 3;
        r4 = [zeros(1,1),y(1:n-1)] * 2;
        r5 = [zeros(1,2),y(1:n-2)];
    end
    
    m = [r1;r2;r3;r4;r5];
    
    for i = 1:n
        v(i) = sum(m(:,i));
    end
    ysmooth = v / 9;
end