function g = gravitationalPull(x)
    g0 = 9.82;   % Given gavitational force
    R = 6.371e6; % Given raduis

    if (R <= x)
        g = g0 * (R^2) / (x^2);
        return;
    
    elseif (0 <= x && x < R)
        g = g0 * x / R;
        return;
    end
end