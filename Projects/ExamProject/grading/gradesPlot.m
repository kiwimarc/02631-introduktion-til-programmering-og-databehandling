function gradesPlot(grades)
    % Plots two plots:
    % 1: A bar plot of the number of students who have received
    %    each of possible final grades on the 7-step-scale
    % 2: A plot with the assignments on the x-axis and the grades
    %    on the y-axis
    %
    % Usage: gradesPlot(grades)
    % 
    % Input grades: An N × M matrix (table) containing grades (doubles)
    %                     for N students on M different assignments
    % Output Display 2 plots

    % Instantiate variables
    % Get all final grades
    finalGrades = computeFinalGrades(grades);
    % Define grades in the 7 grade scale
    posibleGrades = [-3,0,2,4,7,10,12];
    % Define array to hold number of students who have received
    % each of possible final grades
    gradesDistribution = zeros(1,7);
    % Define x-axis labels for plot 2 with prearray
    xLabels = strings(1,width(grades));
    % Define y coordinates array for plot 2
    yCoordinates = zeros(length(grades),height(grades));

    % ------------------------------------------------------------------
    % 1. bar plot
    % Find the grade distribution by looping over all posible grade
    % and counting the instances in finalgrade
    for i = 1:length(posibleGrades)
        count = sum(posibleGrades(i)==finalGrades);
        gradesDistribution(i) = count;
    end
    % Create a new figure window
    figure;
    % Plot bar plot
    bar(posibleGrades, gradesDistribution);
    % Set the title of the graph
    title('Bar blot of final grades');
    % Set the x-axis label
    xlabel('Grades');
    % Set the y-axis label
    ylabel('Number of students');
    yticks(1:length(grades))
    axis padded;
    % Open the plot now
    drawnow;
    % ------------------------------------------------------------------
    % 2. A plot with the assignments on the x-axis and the grades
    %    on the y-axis
    
    % Create a new figure window
    figure;
    % Set the title of the graph
    title('Distribution and average grades per assignments');
    % Set the x-axis label
    xlabel('Assigments');
    % Set the y-axis label
    ylabel('Grades');
    % Set y-axis to follow grades
    yticks(posibleGrades);
    axis padded;
    % Retain current plot when adding new plots
    hold on;
    for i = 1:width(grades)
        % Create x-axis labels
        xLabels(i) = "Assigment "+i;
        x = i*ones(1,length(grades));
        % Fill y coordinates
        for l = 1:height(grades)
            % If not a number is found, then just 
            if isnan(grades(l,i))
                seperator = "#########################################";
                % Making sure the bounds of the box are always right
                % strlength(seperator) - strlength("#  row #) = 33
                spaces = 33-strlength(string(l)+string(xLabels(i)));
                disp(seperator);
                disp("# Not a number was found in the grades! #");
                disp("# "+xLabels(i)+" row "+l+repmat(' ', [1, spaces])+"#");
                disp("# Inserting -3                          #")
                disp(seperator);
                yCoordinates(l,i) = -3;
                continue;
            end
            if ~(sum(yCoordinates(i,:)==0) == length(yCoordinates)) && ismember(grades(l,i),yCoordinates(i,:))
                % While y coordinate are already in array
                while true
                    number = randomNumber();
                    if ~ismember((number+grades(l,i)),yCoordinates(:,i))
                        break
                    end
                end
            else
                number = 0;
            end
            yCoordinates(l,i) = number+grades(l,i);
            x(l) = x(l)+number;
        end
        color = [rand(1,3)];
        if isempty(grades)
            continue
        end
        plot(x,yCoordinates(:,i),".",'Color',color, ...
                                            'DisplayName',xLabels(i));
        plot(x(i),mean(yCoordinates(:,i)),"_",'Color',color, ...
                                            'DisplayName', ...
                                            "Average - "+ xLabels(i), ...
                                            'MarkerSize',20, ...
                                            'LineWidth',5);
    end
    legend();
    xticks(1:width(grades));
    xticklabels(xLabels);
    hold off;
     % Open the plot now
    drawnow;
end

function number = randomNumber()
    % Returns a pseudorandom random number
    % between -0.1 and 0.1
    %
    % Usage: number = randomNumber()
    %
    % Input None
    % Output number: number (double)
    number = (rand(1,1)-0.5)/5;
end