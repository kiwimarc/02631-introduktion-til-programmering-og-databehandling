function freq = letterFrequency(filename)
    txt = fileread(filename); % Read file
    freq = zeros(1,26); % 1x26 array filled with 0
    n = 0;

    freq(1) = count(txt,'a','IgnoreCase',true);
    freq(2) = count(txt,'b','IgnoreCase',true);
    freq(3) = count(txt,'c','IgnoreCase',true);
    freq(4) = count(txt,'d','IgnoreCase',true);
    freq(5) = count(txt,'e','IgnoreCase',true);
    freq(6) = count(txt,'f','IgnoreCase',true);
    freq(7) = count(txt,'g','IgnoreCase',true);
    freq(8) = count(txt,'h','IgnoreCase',true);
    freq(9) = count(txt,'i','IgnoreCase',true);
    freq(10) = count(txt,'j','IgnoreCase',true);
    freq(11) = count(txt,'k','IgnoreCase',true);
    freq(12) = count(txt,'l','IgnoreCase',true);
    freq(13) = count(txt,'m','IgnoreCase',true);
    freq(14) = count(txt,'n','IgnoreCase',true);
    freq(15) = count(txt,'o','IgnoreCase',true);
    freq(16) = count(txt,'p','IgnoreCase',true);
    freq(17) = count(txt,'q','IgnoreCase',true);
    freq(18) = count(txt,'r','IgnoreCase',true);
    freq(19) = count(txt,'s','IgnoreCase',true);
    freq(20) = count(txt,'t','IgnoreCase',true);
    freq(21) = count(txt,'u','IgnoreCase',true);
    freq(22) = count(txt,'v','IgnoreCase',true);
    freq(23) = count(txt,'w','IgnoreCase',true);
    freq(24) = count(txt,'x','IgnoreCase',true);
    freq(25) = count(txt,'y','IgnoreCase',true);
    freq(26) = count(txt,'z','IgnoreCase',true);

    for i = 1:26
        n = n + freq(i);
    end
    freq = (freq / n) * 100;
end