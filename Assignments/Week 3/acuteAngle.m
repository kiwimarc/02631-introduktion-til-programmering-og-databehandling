function theta = acuteAngle(v1, v2)
    
    % If vector is over 2-dimensional 
    if (length(v1) > 2)
        % Compute the first dot product
        theta = v1(1) * v2(1);
        % Compute the rest of the dot product
        for i = 2:length(v1)
            theta = theta + v1(i) * v2(i);
        end
        % Compute arccos to finde theta
        theta = acos(theta);

    else
        % Compute theta from a 2-dimensional vector
        theta = acos(v1(1) * v2(1) + v1(2) * v2(2));
    end

    % Check if theta is bigger than pi/2
    if (theta > pi/2)
        theta = pi - theta;
        return;
    end
end