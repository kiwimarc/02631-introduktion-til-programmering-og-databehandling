function main()
    % Holds the proccess of the program and calls the relevant function
    % depending on chosen menu item.

    % Instantiate variables
    %
    % Define menu items
    menuItems = {'Load new data', 'Check for data errors', 'Generate plots', 'Display list of grades', 'Quit'};
    % Define choice with prevariable
    choice = 1;
    % Define data with prearray
    data = [];
    % define grades witj prearray
    grades = [];

    % Start
    while true
        % Only display menu when user hasn't choosen a option
        % or has requsted to go back to the menu.
        if (choice == 0)
            % Display menu options and ask user to choose a menu item
            choice = displayMenu(menuItems);
        end
        % Menu item chosen
        % ------------------------------------------------------------------
        % 1. Load new data
        if (choice == 1)
           % Get the filename or request to go back to the start menu
           filename = input('Please input filename: ',"s");
           % When input are empty
           if (isempty(filename))
                disp("Input was empty");
           else
               data = dataLoad(filename);
               if isnumeric(data) && isnan(data)
                    disp(filename + " isn't in the root of the project");          
               else
                    choice = 0;
               end
               grades = [];
           end
           % Create grade array to be a N × M matrix for N students
           % on M different assignments
           for i = 3:width(data)

               % Catches errors when strings are in the grade cells
               % If not the program restarts and prompts the user to input
               % new file
               try
                   grades = [grades,table2array(data(:,i))];
               catch
                   disp('Error loading grades for assignment ' +string(i) + ' please check that all values are numbers on the 7 step scale');
                   choice = 1;
                   grades = [];
                   data = [];
                   
               end
           end
           displayseparator();
        % ------------------------------------------------------------------
        % 2. Check for data errors
        elseif (choice == 2)
            errorChecker(data);
            displayseparator();
            choice = 0;
        % ------------------------------------------------------------------
        % 3. Generate plots
        elseif (choice == 3)
            gradesPlot(grades);
            displayseparator();
            choice = 0;
        % ------------------------------------------------------------------
        % 4. Display list of grades
        elseif (choice == 4)
            displayGrades(data,grades)
            displayseparator();
            choice = 0;
        % ------------------------------------------------------------------
        % 5. Quit
        elseif (choice == 5)
            % End
            break
        end
    end
end
function displayseparator()
    % Display a long line for separating menues
    %
    % Usage: displayLine()
    % 
    % Input: None
    % Output: void
    
    disp("-----------------------------------------------------------------------------------------------------------");
end