function sudokuRow = fillSudokuRow(sudokuRow)
    completedRow = [1,2,3,4,5,6,7,8,9];
    
    for i = 1:length(completedRow)
        if ~any(sudokuRow(:) == completedRow(i))
            index = find(sudokuRow == 0);
            sudokuRow(index) = completedRow(i);
            return;
        end
    end
end
    