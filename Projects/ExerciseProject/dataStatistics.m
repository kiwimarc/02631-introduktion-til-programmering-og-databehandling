function result = dataStatistics(data, statistic)
    % Gets data and a user defined statistic that
    % it should the correct statistics on and return
    % a scalar with the calculated statistic

    % Calculate the mean temperature
    if (strcmp(statistic,'Mean Temperature'))
        result = mean(data(:,1));

    % Calculate the mean growth rate
    elseif(strcmp(statistic,'Mean Growth rate'))
        result = mean(data(:,2));

    % Calculate the standard deviation of the temperature
    elseif(strcmp(statistic,'Std Temperature'))
        result = std(data(:,1));

    % Calculate the standard deviation of the growth rate
    elseif(strcmp(statistic,'Std Growth rate'))
        result = std(data(:,2));
    
    % Calculate the number of rows    
    elseif(strcmp(statistic,'Rows'))
        result = height(data);
    
    % Calculate the mean cold growth rate
    elseif(strcmp(statistic,'Mean Cold Growth rate'))
        % Index of all temperatures over or equal to 20
        index = data(:,1) < 20;
        
        result = mean(data(index,2));

    % Calculate the mean hot growth rate
    elseif(strcmp(statistic,'Mean Hot Growth rate'))
        % Index of all temperatures over or equal to 20
        index = data(:,1) > 50;
        
        result = mean(data(index,2));
    end
end

