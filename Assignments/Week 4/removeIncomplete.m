function idComplete = removeIncomplete(id)
    % find highest experiment number
    n = max(floor(id));
    % loop through all the experiments
    for i = 1:n
        % Find all sub experiments
        ids = int16(mod(id(floor(id) == i),i)*10);
        % loop through all sub experiments
        for l = 1:3
            % if not a sub experiment are present in array
            if (~any(ids(:) == l))
                 % Find index of experiment
                 index = find(floor(id) == i);
                 % Remove experiment
                 id(index) = [];
            end
        end
    end
    idComplete = id;
end