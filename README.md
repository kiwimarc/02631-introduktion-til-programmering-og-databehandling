# Introduktion til programmering og databehandling 2022

This archive tries to sum up all curriculum of DTU course [02631 Introduktion til programmering og databehandling](https://kurser.dtu.dk/course/2022-2023/02631) 2022.

To fully understand code comments, roadmap ect. you should be a part of the DTU course 02631 and therefor have access to the study materials. 

## Installation
- [Matlab](https://www.python.org/) 2022 or higher


## Support
If there are missing anything, then feel free to submit an [issue](https://gitlab.com/kiwimarc/02631-introduktion-til-programmering-og-databehandling/-/issues) or send us an [email](mailto:incoming+kiwimarc-02631-introduktion-til-programmering-og-databehandling-39082378-u09mi3geqn24e1fxx1n7vyc7-issue@incoming.gitlab.com).


## License
[MIT License](https://gitlab.com/kiwimarc/02631-introduktion-til-programmering-og-databehandling/-/blob/main/LICENSE)

Feel free to use this project in anyway you want.

## Contributors
[Arthur](https://gitlab.com/arthur-d42)

## Project status
This project will be worked on until the 7th of december 2022. After this date only contributions and suggestions will be added.

