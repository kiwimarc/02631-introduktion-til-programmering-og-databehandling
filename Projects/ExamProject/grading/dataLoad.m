function data = dataLoad(filename)
    % readtables a .csv file in as an array,
    %
    % Usage: data = datareadtable(filename)
    %
    % Input filename: filename (string)
    % Output data: file (array of strings and doubles)
    
    % readtables with the correct extension with the data from the file
    if (isfile(filename))
        file = readtable(filename, 'Format','auto');
    elseif (isfile(filename+".csv"))
        file = readtable(filename+".csv", 'Format','auto');
    else
        file = nan;
    end
    data = file;
end
