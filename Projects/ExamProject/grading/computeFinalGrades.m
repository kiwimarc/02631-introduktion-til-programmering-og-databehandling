function gradesFinal = computeFinalGrades(grades)
    % Computes final grades for students across all M assignments
    %
    % Usage: gradesFinal = computeFinalGrades(grades)
    %
    % Input grades: array of M x N size
    % Output gradesFinal: vector of 1 x N size

    % Define size of array and number of students/assignments
    [N,M] = size(grades);
    % set all failed students grades to -3
    for i = 1:N
        % if any of the assignments grades are -3
        if any(ismember(-3,grades(i,:)))
        % set all to -3
            grades(i,:) = -3;

        elseif any(ismember(nan,grades(i,:)))
            grades(i,:) = nan;
        end
    end
    
    % For different number of assignements
    switch M
        case 1
            gradesFinal = grades;
        %-------------------------------------------------------------------
        % Case: M>1
        otherwise
            % create empty N x M-1 matrix
            newgrades = zeros(N,M-1);
            % loop all students to remove lowest
            for student = 1:N
                %Get student
                thisStudent = grades(student, :);
                %Find index of the min.
                [~, index] = min(thisStudent);
                % Delete the min element
                thisStudent(index) = [];
                %Put into newgrades
                newgrades(student, :) = thisStudent;
            end
            % round newgrades to nearest on the scale
            gradesFinal = roundGrade(mean(newgrades,2)); 
end