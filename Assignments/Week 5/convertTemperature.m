function T = convertTemperature(T, unitFrom, unitTo)

    switch unitTo
        % From Celsius
        case 'Celsius'
            % To Fahrenheit
            if (strcmp(unitFrom,'Fahrenheit'))
                T = (T - 32) / 1.8;
            % To Kelvin
            elseif (strcmp(unitFrom,'Kelvin'))
                T = T - 273.15;
            end
       % From Fahrenheit 
       case 'Fahrenheit'
           % To Celsius
            if (strcmp(unitFrom,'Celsius'))
                T = 1.8 * T + 32;
            % To Kelvin
            elseif (strcmp(unitFrom,'Kelvin'))
                T = T + 273.75;
            end
        % From Kelvin
        case 'Kelvin'
            % To Celsius
            if (strcmp(unitFrom,'Celsius'))
                T = T + 273.15;
            % To Fahrenheit
            elseif (strcmp(unitFrom,'Fahrenheit'))
                T = (T + 459.67) / 1.8;
            end
    end

end