function displayGrades(data,grades)
    % Displays grades for all assignments and final grades
    % For students in alphabetized order
    %
    % Usage: displayGrade(data, grades)
    %
    % Input data, grades: data (array of strings and doubles), 
    %                     grades (array of doubles)
    % Output prints sorted data to console

    % Append final grades as column to table
    finalGrades = transpose(computeFinalGrades(grades));
    data.finalGrades = finalGrades;
    % Sort by name
    try
        sorted = sortrows(data,{'Name'});
    catch
        sorted = sortrows(data,2);
    end

    disp(sorted);
    if isempty(sorted)
        disp("######################################################");
        disp("#                     Is empty                       #");
        disp("######################################################");
    end
end
