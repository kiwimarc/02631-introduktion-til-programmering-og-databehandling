function season = astronomical_season(date)
    date = split(string(date),"-");
    date = split(date(1),"/");
    if (double(date(2)) >= 3 && double(date(2)) < 6)
        if (double(date(1)) < 20 && double(date(2)) == 3)
            season = "winter";
        else
            season = "spring";
        end
    elseif (double(date(2)) >= 6 && double(date(2)) < 9)
        if (double(date(1)) < 21 && double(date(2)) == 6)
            season = "spring";
        else
            season = "summer";
        end
    elseif (double(date(2)) >= 9 && double(date(2)) < 12)
        if (double(date(1)) < 23 && double(date(2)) == 9)
            season = "summer";
        else
            season = "autumn";
        end
    elseif (double(date(2)) == 12 || double(date(2)) < 3)
        if (double(date(1)) < 21 && double(date(2)) == 12)
            season = "autumn";
        else
            season = "winter";
        end
    end
end