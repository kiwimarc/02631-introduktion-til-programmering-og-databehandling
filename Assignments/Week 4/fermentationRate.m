function averageRate = fermentationRate(measuredRate, lowerBound, upperBound)
    sum = 0;
    n = 0;
    
    % Foreach element of measuredRate vektor
    for i = 1:length(measuredRate)
        % If the rate is higher than lower bound and lower thand upper
        % bound
        if((measuredRate(i) > lowerBound) && (measuredRate(i) < upperBound))
            sum = sum + measuredRate(i);
            n = n+1;
        end
    end
    averageRate = sum/n;
end