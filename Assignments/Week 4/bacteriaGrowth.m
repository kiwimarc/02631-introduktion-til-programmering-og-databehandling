function tN = bacteriaGrowth(n0, alpha, K, N)
    tN = 0;
    nt = n0;
    
    % While bacteria growth are under the given max bacteria growth
    while(nt <= N)
        nt = (1 + alpha * (1 - nt/K)) * nt;
        tN = tN+1;
    end
end
