function data = dataLoad(filename)
    % Loads a .txt or .csv file in as an array,
    % sanitizes array from given specification
    % and returns the sanitized array.
    %
    % Usage: data = dataLoad(filename)
    %
    % Input filename: filename (string)
    % Output data: file (array of doubles)
    
    % Loads file with the correct extension
    % in as a double-precision array
    % with the data from the file
    if (isfile(filename+".txt"))
        file = load(filename+".txt");
    elseif (isfile(filename+".csv"))
        file = load(filename+".csv");
    elseif (isfile(filename))
        file = load(filename);
    elseif (isfile("data/"+filename+".txt"))
        file = load("data/"+filename+".txt");
    elseif (isfile("data/"+filename+".csv"))
        file = load("data/"+filename+".csv");
    elseif (isfile("data/"+filename))
        file = load("data/"+filename);
    end
    
    % Finds indexes of all the errors
    % Temp over 60 and under 10 
    tempIndex = (file(:,1) > 60 | file(:,1) < 10);
    % Growth rate under 0
    grIndex = (~(file(:,2) >= 0));
    % Not a valid bacteria type
    bactIndex = (file(:,3) ~= 1 & file(:,3) ~= 2 & file(:,3) ~= 3 & file(:,3) ~= 4);

    % Display all rows with errors
    format shortG; % Change output to remove 1.0e+04 *
    disp("Error at these data rows:");
    disp("The temperature is out of range: 10 - 60 degrees");
    disp("Row "+ find(tempIndex == 1));
    disp("##############################################################");
    disp("The Grow rate is under 0");
    disp("Row "+ find(grIndex == 1));
    disp("##############################################################");
    disp("The bacteria type is not valid");
    disp("Row "+ find(bactIndex == 1));
    disp("##############################################################");
    disp("The shown data rows are removed from the loaded data");
    
    % Merge all error index arrays to one
    errorIndex = [tempIndex(tempIndex); grIndex(grIndex); bactIndex(bactIndex)];
    % Remove all indices
    file(errorIndex);
    
    disp("There are "+length(file)+" valid rows");
    
    % Return sanitized array
    data = file;
end
