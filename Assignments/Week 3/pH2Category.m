function category = pH2Category(pH)
    if (pH >= 0 && pH < 3)
        category = "Strongly acidic";
        return;

    elseif (pH >= 3 && pH < 6)
        category = "Weakly acidic";
        return;

    elseif (pH >= 6 && pH <= 8)
        category = "Neutral";
        return;
   
    elseif (pH > 8 && pH <= 11)
        category = "Weakly basic";
        return;

    elseif (pH > 11 && pH <= 14)
        category = "Strongly basic";
        return;
    
    else
        category = "pH out of range";
        return;

    end
end