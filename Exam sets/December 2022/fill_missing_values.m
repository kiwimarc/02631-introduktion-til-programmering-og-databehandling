function B = fill_missing_values(A)

    for i = 1:height(A)
        index =  A(i,:) == -1;
        A(i,index) = (sum(A(i,:))+sum(index))/(width(A(i,:))-sum(index)); 
    end
    B = A;
end