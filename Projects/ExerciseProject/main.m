function main()
    % Holds the proccess of the program and calls the relevant function
    % depending on chosen menu item.

    % Instantiate variables
    %
    % Define menu items
    menuItems = {'Load Data', 'Filter Data', 'Display statistics', 'Generate Plots', 'Quit'};
    % Define choice with prevariable
    choice = 0;
    % Define data with prearray
    data = [];
    % Define statistic choice with prevariable
    statChoice = -1;
    % Define statistic result with prescalar
    statResult = 0;
    % Define filter with precell-array
    filter = {};
    % Define start data with prearray
    startData = [];

    % Start
    while true
        % When filter is active, tell user what filter is active
        if (~cellfun(@isempty,filter))
            disp("#############################");
            disp("Following filters are active:");
            for i = 1:length(filter)
                disp(filter{i});
            end
            disp("#############################");
        end
        % Only display menu when user hasn't choosen a option
        % or has requsted to go back to the menu.
        if (choice == 0)
            % Display menu options and ask user to choose a menu item
            choice = displayMenu(menuItems);
        end
        % Menu item chosen
        % ------------------------------------------------------------------
        % 1. Load Data
        if (choice == 1)
            % Check if user want to change data
            if (~isempty(data))
                disp("Data already loaded in");
                % Tell user that a reload of dataset will reset filters
                if(~isempty(filter))
                    disp("A reload of dataset will reset your filters");
                end
                disp("Do you want to load another dataset?");
                % Menu options
                items = {'Yes', 'No'};
                % Display menu options and get user input
                dataChoice = displayMenu(items);
                % User want a reload of dataset
                if (dataChoice == 1)
                    filter = {};
                    data = [];
                % User dont want a reload of dataset
                elseif (dataChoice == 2)
                    choice = 0;
                    displayseparator();
                    continue;
                end
                    
            end
            % Get the filename or request to go back to the start menu
            filename = input('Please input filename or type 0 to go back to the menu: ',"s");

            % When input are empty
            if (isempty(filename))
                disp("Input was empty");
            % When user has requsted to go back
            elseif (strcmp(filename, "0"))
                choice = 0;
            % When file doesn't exist
            elseif ( ~(isfile(filename) || isfile(filename+".txt") || isfile(filename+".csv")) || ...
                    ~(isfile("data/"+filename) || isfile("data/"+filename+".txt") || isfile("data/"+filename+".csv")))
                disp(filename + " isn't in the datafolder or in roots of the project");
                displayseparator();
                continue;
            else
                startData = dataLoad(filename);
                data = startData;
                disp("Data loaded in");
                choice = 0;
            end
            displayseparator();
           
        % ------------------------------------------------------------------
        % 2. Filter Data
        elseif (choice == 2)
            % data not loaded in
            if (isempty(data))
                disp("No data is loaded in yet, please load data in");
                displayseparator();
                choice = 0;
                continue;
            end
            % no filter is active
            if (isempty(filter))
                disp("The availables operators to filter by are:")
                disp("<= >= < > =");
                disp("You can sort by columns:");
                disp("Temperature, Growth rate and Bacteria");
                disp("You can do it by fx:");
                disp("1) Bacteria = Listeria");
                disp("2) 0.5 <= Growth rate <= 1");
                inputString = input('Please input your filter or type 0 to go back to the menu: ',"s");
                % When user has requsted to go back
                if (strcmp(filename, "0"))
                    choice = 0;
                    displayseparator();
                    continue;
                end
                [filter{end+1}, data] = filterData(inputString, data);
                choice = 0;

            elseif (~cellfun(@isempty,filter))
                items = {'Add more filters', 'Deaktivate filter', 'Go back'};
                % Display menu options and get user input
                filterChoice = displayMenu(items);
                % Add more filters
                if (filterChoice == 1)
                    disp("The availables operators to filter by are:")
                    disp("<= >= < > =");
                    disp("You can sort by columns:");
                    disp("Temperature, Growth rate and Bacteria");
                    disp("You can do it by fx:");
                    disp("1) Bacteria = Listeria");
                    disp("2) 0.5 <= Growth rate <= 1");
                    inputString = input('Please input your filter or type 0 to go back to the menu: ',"s");
                    % When user has requsted to go back
                    if (strcmp(filename, "0"))
                        choice = 0;
                        displayseparator();
                        continue;
                    end
                    [filter{end+1}, data] = filterData(inputString, data);
                    choice = 0;

                % Remove a filter or all of them
                elseif (filterChoice == 2)
                    disp("Following filters are active");
                    disp("Choose what filter you want to remove");
                    items = {};
                    % Loop through all filters
                    for i = 1:length(filter)
                       % Add current filter as an option to remove
                       items{end+1} = filter{i};
                    end
                    % Added option to remove all filters
                    items{end+1} = 'Delete all filters';
                    % Go back option
                    items{end+1} = 'Go back';
                    % Display menu options and get user input
                    removeFilterChoice = displayMenu(items);
                    
                    % Remove the selected filter
                    if (removeFilterChoice <= length(filter))
                        disp("Removing the selected filter");
                        startLength = length(filter);
                        filter{removeFilterChoice} = [];
                        % There were more than one filter active
                        if (any(~cellfun(@isempty,filter)))
                            disp("Getting dataset with the rest of the filters");
                            n = length(filter);
                            prefilter = filter;
                            % Resting filter and dataset
                            filter = {};
                            [filter{end+1}, data] = filterData(prefilter{1}, startData);
                            if (startLength-n > 1)
                                for i = 1:length(n)
                                    disp("Filtering for "+ prefilter{i});
                                    [filter{end+1}, data] = filterData(prefilter{i}, data);
                                end
                            end
                            disp("Filter removed");
                            disp("You have "+length(data)+" rows");
                            choice = 0;
                            displayseparator();
                            continue;
                        % There were only one active filter
                        else 
                            filter = {};
                            data = startData;
                        end
                        disp("Filter removed");
                        disp("You have "+length(data)+" rows");

                    elseif (removeFilterChoice == length(filter) + 1)
                        disp("Removing all filters");
                        disp("You have "+length(startData)+" rows");
                        data = startData;
                        filter = {};
                        filterChoice = 0;
                        removeFilterChoice = 0;
                        choice = 0;
                        displayseparator();
                        continue;

                    elseif (removeFilterChoice == length(filter) + 2)
                        filterChoice = 0;
                        displayseparator();
                        continue;
                    end


                % Go back to main menu
                elseif (filterChoice == 3)
                    choice = 0;
                    displayseparator();
                    continue;
                end
            end
            displayseparator();

        % ------------------------------------------------------------------
        % 3. Display statistics
        elseif (choice == 3)
            % data not loaded in
            if (isempty(data))
                disp("No data is loaded in yet, please load data in");
                displayseparator();
                choice = 0;
                continue;
            end
            % 
            items = {'Mean Temperature', 'Mean Growth rate', 'Std Temperature', 'Std Growth rate', 'Rows', 'Mean Cold Growth rate', 'Mean Hot Growth rate', 'Go back'};
            % Display menu options and get user input
            statChoice = displayMenu(items);

            % 1. Mean Temperature
            if(statChoice == 1)
                statResult = dataStatistics(data, 'Mean Temperature');
                disp('The mean temperature is: ' + string(statResult));
                choice = 0;
                statChoice = -1;

            % 2. Mean Growth rate
            elseif(statChoice == 2)
                statResult = dataStatistics(data, 'Mean Growth rate');
                disp('The growth rate is: ' + string(statResult));
                choice = 0;
                statChoice = -1;
            
            % 3. Std Temperature
            elseif(statChoice == 3)
                statResult = dataStatistics(data, 'Std Temperature');
                disp('The standard deviation of the temperature is: ' + statResult);
                choice = 0;
                statChoice = -1;

            % 4. Std Growth rate
            elseif(statChoice == 4)
                statResult = dataStatistics(data, 'Std Growth rate');
                disp('The standard deviation of the growth rate is: ' + string(statResult));
                choice = 0;
                statChoice = -1;

            % 5. Rows
            elseif(statChoice == 5)
                statResult = dataStatistics(data, 'Rows');
                disp('The total row count is: ' + string(statResult));
                choice = 0;
                statChoice = -1;

            % 6. Mean Cold Growth rate
            elseif(statChoice == 6)
                statResult = dataStatistics(data, 'Mean Cold Growth rate');
                disp('The mean cold growth rate is: ' + string(statResult));
                choice = 0;
                statChoice = -1;
          
            % 7. Mean Hot Growth rate
            elseif(statChoice == 7)
                statResult = dataStatistics(data, 'Mean Hot Growth rate');
                disp('The mean hot growth rate is: ' + string(statResult));
                choice = 0;
                statChoice = -1;

            % 8. User want to go back
            elseif (statChoice == 8)
                choice = 0;
                statChoice = -1;

            end
            displayseparator();
        % ------------------------------------------------------------------
        % 4. Generate Plots
        elseif (choice == 4)
            % data not loaded in
            if (isempty(data))
                disp("No data is loaded in yet, please load data in");
                displayseparator();
                choice = 0;
                continue;
            end
            dataPlot(data);
            choice = 0;
            displayseparator();
            
        % ------------------------------------------------------------------
        % 5. Quit
        elseif (choice == 5)
            % End
            break
        end
    end
end
function displayseparator()
    % Display a long line for separating menues
    %
    % Usage: displayLine()
    % 
    % Input: None
    % Output: void
    
    disp("-----------------------------------------------------------------------------------------------------------");
end