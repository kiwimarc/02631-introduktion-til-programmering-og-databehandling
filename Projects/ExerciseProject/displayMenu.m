function choice = displayMenu(options)
    % Orginal forfatter: Mikkel N. Schmidt, mnsc@dtu.dk, 2015
    % Ændret af: Marc Cummings
    % Displays a menu of options in the terminal, asks the user to
    % choose an option and returns the number of the menu item chosen.
    %
    % Usage: choice = displayMenu(options)
    %
    % Input options: Menu options (cell array of strings)
    % Output choice: Chosen option (integer)

    % Display menu options
    for i = 1:length(options)
        disp(string(i)+'. '+options{i});
    end
    % Get a valid menu choice
    choice = 0;
    while ~any(choice == 1:length(options))
        choice = input('Please choose a menu item: ');
    end
end