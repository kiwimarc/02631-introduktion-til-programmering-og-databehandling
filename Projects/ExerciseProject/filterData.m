function [filter, filteredData] = filterData(inputFilter, data)
    % Filters data from user defined operatore 
    %
    % Usage: [filter data] = filterData(inputFilter)
    %
    % Input inputFilter, data: user input (string), loaded data (array of doubles)
    % Output [filter data]: [All filters (array)   the filtered data (array)]
    
    % Define filter with prearray
    filter = [];
    % Define filteredData with prearray
    filteredData = [];
    % Save input filter for storing in filter array
    orginalInput = inputFilter;

    % Posible operatore
    operatore = ["<=" ">=" "<" ">" "="];
    % Lower case the whole string
    inputFilter = lower(inputFilter);
    if (contains(inputFilter, operatore))
        % Loop through all operatores 
        for i = 1:length(operatore)
            % Remove whitespaces
            inputFilter = strrep(inputFilter,' ','');
            % If operatore is in the inputfilter
            if (contains(inputFilter, operatore(i)))
                % Change = to ==, so it work in eval function
                if (strcmp(operatore(i), "="))
                    inputFilter = strrep(inputFilter,"=","==");
                    operatore(i) = "==";
                end
                % Splits input string by the operatore into array
                prefilter = split(inputFilter, operatore(i));
                % If no operatores is in the first element of the array
                if (~contains(prefilter(1), operatore))
                    % Add first part of array(input string) and the operatore to filter
                    % array
                    filter = [filter; [prefilter(1); operatore(i)]];
                    % If rest of array(input string) still contains operatores
                    if (contains(prefilter(2:end), operatore))
                        inputFilter = strjoin(prefilter(2:end));
                    % If the length of the rest of array(input string) are over 2
                    elseif(length(prefilter(2:end)) >= 2)
                        prefilter = prefilter(2:end);
                        for l = 1:length(prefilter)
                            % If its the end of array
                            if (l == length(prefilter)) 
                                % Just add the end of input string
                                filter = [filter; prefilter(l)];
                            else
                                % Add both the operator and the string 
                                filter = [filter; [prefilter(l); operatore(i)]];
                            end
                        end
                        break;
                    else
                        % Just add the end of the input string
                        filter = [filter; prefilter(2)];
                        break
                    end
                % if no operatore is in the end of the string
                elseif (~contains(prefilter(2), operatore))
                    % Run first part of the string through the function again
                    % and add the operatore and the end of the string
                    filter = [filter; filterData(prefilter(1)); operatore(i); prefilter(2)];
                    break
                else
                    % Run first part of the string through the function again
                    filter = [filter; filterData(prefilter(1))];
                    inputFilter = strjoin([operatore(i) prefilter(2:end)]);
                end
            end
        end
    elseif (~contains(inputFilter, operatore))
        disp("No valid operators are found");
        filteredData = data;
        return;
    end
    
    % Possible filters 
    fields = ["temperature" "growthrate" "bacteria"];
    % if there are more than one filter, then display error
    if (sum(count(filter,fields)) > 1)
        disp("To many fields to filter for");
        disp("Please only type one fields per input");
        return;
    end
    % Find index of filter
    for i = 1:length(fields)
        if (~isempty(filter(strcmp(filter,fields(i)))))
            filter(strcmp(filter,fields(i))) = i;
            break;
        end
    end
    % Switch case for the filter
    switch fields(i)
        % Temperature is in column 1
        case 'temperature'
            %  Run through all the data
            for i = 1:length(data)
                if (length(filter) > 3)
                    if (eval(filter(1)+filter(2)+data(i,1)) == 1 && eval(data(i,1)+filter(4)+filter(5)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                else
                    if (eval(filter(1)+filter(2)+data(i,1)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                end
            end
        % Temperature is in column 2
        case 'growthrate'
            for i = 1:length(data)
                if (length(filter) > 3)
                    if (eval(filter(1)+filter(2)+data(i,2)) == 1 && eval(data(i,2)+filter(4)+filter(5)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                else
                    if (eval(filter(1)+filter(2)+data(i,2)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                end
            end
        % Temperature is in column 3    
        case 'bacteria'
            % Possible bacterias 
            bacterias = ["salmonellaenterica" "bacilluscereus" "listeria" "brochothrixthermosphacta"];
            % Find index of bacteria
            for l = 1:length(bacterias)
                if (~isempty(filter(strcmp(filter,bacterias(l)))))
                    filter(strcmp(filter,bacterias(l))) = l;
                    break;
                end
            end
            for i = 1:length(data)
                if (length(filter) > 3)
                    if (eval(filter(1)+filter(2)+data(i,3)) == 1 && eval(data(i,3)+filter(4)+filter(5)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                else
                    if (eval(filter(1)+filter(2)+data(i,3)) == 1)
                        filteredData = [filteredData; data(i,:)];
                    end
                end
            end
    end
    % When no filtering was done
    if (isempty(filteredData))
        disp("No hits for your desired filter");
        disp("Returning last dataset");
        disp("Likely cause: conflict between filters");
        filteredData = data;
        filter = [];
    % Tell user that filtering was done and how many rows are left
    else
        disp("Data is filtered");
        disp("There were removed "+(length(data)-length(filteredData))+" rows");
        filter = orginalInput;
    end
end