function A = boxArea(boxCorners, area)
    % Split coordinates 
    Box1 = [boxCorners(1:2),boxCorners(5:6)];
    Box2 = [boxCorners(3:4),boxCorners(7:8)];

    % Use a switch statement to choose between the different areas to compute.
    switch area
        case 'Box1'
            A = (Box1(2) - Box1(1))*(Box1(4) - Box1(3));

        case 'Box2'
            A = (Box2(2) - Box2(1))*(Box2(4) - Box2(3));

        case 'Intersection'
            A = max(0,min(Box1(2),Box2(2)) - max(Box1(1),Box2(1)))*max(0,min(Box1(4),Box2(4)) - max(Box1(3),Box2(3)));

        case 'Union'
            A = (Box1(2) - Box1(1))*(Box1(4) - Box1(3)) + (Box2(2) - Box2(1))*(Box2(4) - Box2(3)) - max(0,min(Box1(2),Box2(2)) - max(Box1(1),Box2(1)))*max(0,min(Box1(4),Box2(4)) - max(Box1(3),Box2(3)));
    end
end