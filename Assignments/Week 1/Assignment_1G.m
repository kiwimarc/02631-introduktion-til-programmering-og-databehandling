b = 12;
c = 10;
A = 0.25*pi;

%Computing the length of the side a using the cosine rule.
a = sqrt(b^2+c^2-2*b*c*cos(A));

% NOT IN THE ASSIGNMENT ONLY TO CHECK RESULT
disp(a)